package fr.samadou.myerp.invoice.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static fr.samadou.myerp.invoice.domain.InvoicesFixture.*;

import org.junit.jupiter.api.Test;

import fr.samadou.myerp.UnitTest;


@UnitTest
class InvoiceTest {

	@Test
	void shouldGetInvoiceInformation() {
		var invoice = invoice();

		assertThat(invoice.lines())
		.containsExactly(firstLine(), secondLine());

		assertThat(invoice.total())
		.isEqualTo(fee());
	}
}
