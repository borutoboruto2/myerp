package fr.samadou.myerp.invoice.domain;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public final class InvoicesFixture {

	private InvoicesFixture() {

	}

	public static Invoice invoice() {
		return new Invoice(invoiceId(), List.of(firstLine(), secondLine()));
	}

	public static InvoiceId invoiceId() {
		return new InvoiceId(UUID.fromString("DEB49C37-A2ED-4862-826E-4B5CF02BCCAB"));
	}

  public static InvoiceToCreate invoiceToCreate() {
    return new InvoiceToCreate(lines());
  }

  private static List<Line> lines() {
    return List.of(firstLine(), secondLine());
  }

	public static Line firstLine() {
		return line(2, 1100);
	}

	public static Line secondLine() {
		return line(1, 550);
	}

	public static Line line(int quantity, int unitPrice) {
		return new Line(new Quantity(quantity), fee(unitPrice));
	}

  private static Fee fee(int unitPrice) {
    return new Fee(amount(unitPrice), Currency.EURO);
  }

  public static Fee fee() {
    return fee(2750);
  }

  private static Amount amount(int value) {
    return new Amount(value);
  }

}
