package fr.samadou.myerp.invoice.domain;

import fr.samadou.myerp.UnitTest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sareaboudousamadou.
 */
@UnitTest
class InvoiceIdTest {

  @Test
  void shouldGenerateIds() {
    Assertions.assertThat(InvoiceId.newId())
      .isNotNull()
      .isNotEqualTo(InvoiceId.newId());
  }
}
