package fr.samadou.myerp.invoice.infrastructure.primary;

import fr.samadou.myerp.JsonHelper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static fr.samadou.myerp.invoice.domain.InvoicesFixture.invoiceToCreate;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sareaboudousamadou.
 */
class InvoicesRessourceTest {

  @Test
  void shouldConvertToDomain() {
    Assertions.assertThat(JsonHelper.readFromJson(json(), RestInvoiceToCreate.class)
      .toDomain())
      .usingRecursiveComparison()
      .isEqualTo(invoiceToCreate());
  }

  private String json() {


    return """

      {
        "lines": [
          {
            "quantity": 2,
            "unitPrice": {
              "amount": 1100,
              "currency": "EURO"
            }
          },

          {
            "quantity": 1,
            "unitPrice": {
              "amount": 550,
              "currency": "EURO"
            }
          }

        ]

      }
      """;
  }
}
