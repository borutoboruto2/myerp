package fr.samadou.myerp.invoice.infrastructure.primary;

import fr.samadou.myerp.JsonHelper;
import fr.samadou.myerp.UnitTest;
import fr.samadou.myerp.invoice.domain.InvoicesFixture;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author sareaboudousamadou.
 */
@UnitTest
class RestInvoiceTest {

  @Test
  void shouldSerializeToJson() {
    Assertions.assertThat(JsonHelper.writeAsString(RestInvoice.from(InvoicesFixture.invoice())))
      .isEqualTo(json());
  }

  private String json() {
    return """
      {\
      "id":"deb49c37-a2ed-4862-826e-4b5cf02bccab",\
      "lines":[\
      {"quantity":2,"restFee":{"amount":1100,"currency":"EURO"}},\
      {"quantity":1,"restFee":{"amount":550,"currency":"EURO"}}\
      ],\
      "total":2750}\
      """;
  }
}
