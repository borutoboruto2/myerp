package fr.samadou.myerp.invoice.infrastructure.primary;

import fr.samadou.myerp.cucumber.CucumberAssertions;
import fr.samadou.myerp.cucumber.CucumberRestTemplate;
import fr.samadou.myerp.cucumber.CucumberTestContext;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InvoicesSteps {


	private static final String DEFAULT_INVOICE_PAYLOAD = """
			{

			"lines": [
			{
			"quantity": 2,
			"unitPrice": 1100
			},
			{
			"quantity": 1,
			"unitPrice": 550
			}

			]

			}
			""";

	@Autowired
	private CucumberRestTemplate rest;

  @Given("I create invoice")
  public void createInvoice(List<Map<String, String>> lines) {
    String linesPayload = lines.stream()
      .map(line -> "{\"quantity\":" + line.get("Quantity") + ", \"unitPrice\": {\"amount\":" + line.get("Unit price")
        + ", \"currency\": \"EURO\"}" +
        "}").collect(Collectors.joining(","));

    String payload = "{\"lines\":[" + linesPayload + "]}";

    rest.post("/api/invoices", payload);
  }

  @When("I get created invoice")
  public void getCreatedInvoice() {
    rest.get("/api/invoices/" + CucumberTestContext.getElement("$.id"));
  }

  @Then("I should have invoice")
  public void shouldHaveInvoice(Map<String, String> invoice) {
    CucumberAssertions.assertThatLastAsyncResponse()
      .hasOkStatus()
      .hasResponse()
      .containing(invoice);
  }
}
