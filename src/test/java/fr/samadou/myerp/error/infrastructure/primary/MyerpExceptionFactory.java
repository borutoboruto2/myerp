package fr.samadou.myerp.error.infrastructure.primary;

import fr.samadou.myerp.error.domain.MyerpException;

public final class MyerpExceptionFactory {

  private MyerpExceptionFactory() {}

  public static final MyerpException buildEmptyException() {
    return MyerpException.builder(null).build();
  }
}
