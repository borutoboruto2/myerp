package fr.samadou.myerp.error.infrastructure.primary;

import static org.mockito.Mockito.*;

import ch.qos.logback.classic.Level;
import fr.samadou.myerp.LogsSpy;
import fr.samadou.myerp.UnitTest;
import fr.samadou.myerp.error.domain.MyerpException;
import fr.samadou.myerp.error.domain.StandardErrorKey;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.MessageSource;

@UnitTest
@ExtendWith(LogsSpy.class)
class MyerpErrorsHandlerTest {

  private static final MyerpErrorsHandler handler = new MyerpErrorsHandler(mock(MessageSource.class));

  private final LogsSpy logs;

  public MyerpErrorsHandlerTest(LogsSpy logs) {
    this.logs = logs;
  }

  @Test
  void shouldLogServerErrorAsError() {
    handler.handleMyerpException(MyerpException.internalServerError(StandardErrorKey.INTERNAL_SERVER_ERROR).message("Oops").build());

    logs.shouldHave(Level.ERROR, "Oops");
  }

  @Test
  void shouldLogClientErrorAsInfo() {
    handler.handleMyerpException(MyerpException.badRequest(StandardErrorKey.BAD_REQUEST).message("Oops").build());

    logs.shouldHave(Level.INFO, "Oops");
  }
}
