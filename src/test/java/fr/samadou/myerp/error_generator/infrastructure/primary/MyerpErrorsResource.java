package fr.samadou.myerp.error_generator.infrastructure.primary;

import fr.samadou.myerp.error.domain.MyerpException;
import fr.samadou.myerp.error.domain.StandardErrorKey;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/errors")
class MyerpErrorsResource {

  @GetMapping("bad-request")
  void getBadRequest() {
    throw MyerpException.badRequest(StandardErrorKey.BAD_REQUEST).addParameter("code", "400").build();
  }
}
