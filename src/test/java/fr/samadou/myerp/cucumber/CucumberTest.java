package fr.samadou.myerp.cucumber;

import fr.samadou.myerp.ComponentTest;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@ComponentTest
@RunWith(Cucumber.class)
@CucumberOptions(
  glue = "fr.samadou.myerp",
  plugin = {
    "pretty", "json:target/cucumber/cucumber.json", "html:target/cucumber/cucumber.htm", "junit:target/cucumber/TEST-cucumber.xml",
  },
  features = "src/test/features"
)
public class CucumberTest {}
