package fr.samadou.myerp.invoice.infrastructure.primary;

import fr.samadou.myerp.invoice.application.InvoiceApplicationService;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * @author sareaboudousamadou.
 */
@RestController
@RequestMapping("/api/invoices")
public class InvoicesRessource {

  private final InvoiceApplicationService invoices;

  public InvoicesRessource(InvoiceApplicationService invoices) {
    this.invoices = invoices;
  }

  @PostMapping
  ResponseEntity<RestInvoice> create(@Valid @RequestBody RestInvoiceToCreate invoiceToCreate) {
    RestInvoice restInvoice = RestInvoice.from(invoices.create(invoiceToCreate.toDomain()));

    return new ResponseEntity<>(restInvoice, HttpStatus.CREATED);
  }

  @GetMapping("/{invoice-id}")
  ResponseEntity<RestInvoice> get(@PathVariable("invoice-id") UUID invoiceId) {

    return ResponseEntity.of(invoices.get(invoiceId)
      .map(RestInvoice::from));
  }
}
