package fr.samadou.myerp.invoice.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.samadou.myerp.invoice.domain.Invoice;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author sareaboudousamadou.
 */
public class RestInvoice {
  private final UUID id;
  private final Collection<RestLine> lines;
  private final BigDecimal total;

  public RestInvoice(@JsonProperty("id") UUID id,
                     @JsonProperty("lines") Collection<RestLine> lines,
                     @JsonProperty("total") BigDecimal total) {
    this.id = id;
    this.lines = lines;
    this.total = total;
  }

  public static RestInvoice from(Invoice invoice) {
    return new RestInvoice(invoice.id().get(), invoice.lines().stream()
      .map(RestLine::fromDomain).toList(),
      invoice.total().amount().get());
  }

  public UUID getId() {
    return id;
  }

  public Collection<RestLine> getLines() {
    return lines;
  }

  public BigDecimal getTotal() {
    return total;
  }
}
