package fr.samadou.myerp.invoice.infrastructure.secondary;

import fr.samadou.myerp.error.domain.Assert;
import fr.samadou.myerp.invoice.domain.Invoice;
import fr.samadou.myerp.invoice.domain.InvoiceId;
import fr.samadou.myerp.invoice.domain.InvoiceRepository;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author sareaboudousamadou.
 */
@Repository
public class InMemoryInvoicesRepository implements InvoiceRepository {

  private final Map<InvoiceId, Invoice> invoices = new ConcurrentHashMap<>();

  @Override
  public void save(Invoice invoice) {
    Assert.notNull("invoice", invoice);

    invoices.put(invoice.id(), invoice);
  }

  @Override
  public Optional<Invoice> get(InvoiceId invoiceId) {
    Assert.notNull("invoiceId", invoiceId);

    return Optional.ofNullable(invoices.get(invoiceId));
  }
}
