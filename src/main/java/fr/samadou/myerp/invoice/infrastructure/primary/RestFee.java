package fr.samadou.myerp.invoice.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.samadou.myerp.invoice.domain.Amount;
import fr.samadou.myerp.invoice.domain.Currency;
import fr.samadou.myerp.invoice.domain.Fee;

import java.math.BigDecimal;

/**
 * @author sareaboudousamadou.
 */
public class RestFee {
  private final BigDecimal amount;
  private final Currency currency;

  public RestFee(@JsonProperty("amount") BigDecimal amount,
                 @JsonProperty("currency") Currency currency) {
    this.amount = amount;
    this.currency = currency;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public Currency getCurrency() {
    return currency;
  }

  public Fee toDomain() {
    return new Fee(new Amount(amount.intValue()), currency);
  }

  public static RestFee fromDomain(Fee fee) {
    return new RestFee(fee.amount().get(), fee.currency());
  }
}
