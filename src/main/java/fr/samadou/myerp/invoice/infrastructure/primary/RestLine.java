package fr.samadou.myerp.invoice.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.samadou.myerp.invoice.domain.Line;
import fr.samadou.myerp.invoice.domain.Quantity;

/**
 * @author sareaboudousamadou.
 */
public class RestLine {
  private final int quantity;

  private final RestFee restFee;

  public RestLine(@JsonProperty("quantity") int quantity,
                  @JsonProperty("unitPrice") RestFee restFee) {
    this.quantity = quantity;
    this.restFee = restFee;
  }

  public Line toDomain() {
    return new Line(new Quantity(quantity), restFee.toDomain());
  }

  public static RestLine fromDomain(Line line) {
    return new RestLine(line.quantity().value(), RestFee.fromDomain(line.fee()));
  }

  public int getQuantity() {
    return quantity;
  }

  public RestFee getRestFee() {
    return restFee;
  }
}
