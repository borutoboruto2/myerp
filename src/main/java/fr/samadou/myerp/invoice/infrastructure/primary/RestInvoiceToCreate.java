package fr.samadou.myerp.invoice.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import fr.samadou.myerp.invoice.domain.InvoiceToCreate;

import java.util.Collection;

/**
 * @author sareaboudousamadou.
 */
public class RestInvoiceToCreate {

  private final Collection<RestLine> lines;

  public RestInvoiceToCreate(@JsonProperty("lines") Collection<RestLine> lines) {
    this.lines = lines;
  }

  public Collection<RestLine> getLines() {
    return lines;
  }

  public InvoiceToCreate toDomain() {
    return new InvoiceToCreate(lines.stream().map(RestLine::toDomain).toList());
  }
}
