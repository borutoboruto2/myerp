package fr.samadou.myerp.invoice.application;

import fr.samadou.myerp.invoice.domain.*;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

/**
 * @author sareaboudousamadou.
 */
@Service
public class InvoiceApplicationService {

  private final InvoiceCreator creator;
  private final InvoiceRepository invoices;

  public InvoiceApplicationService(InvoiceRepository invoices) {
    this.creator = new InvoiceCreator(invoices);
    this.invoices = invoices;
  }


  public Invoice create(InvoiceToCreate invoiceToCreate) {
    return creator.create(invoiceToCreate);
  }

  public Optional<Invoice> get(UUID invoiceId) {
    return invoices.get(new InvoiceId(invoiceId));
  }
}
