package fr.samadou.myerp.invoice.domain;


import fr.samadou.myerp.error.domain.Assert;

/**
 * @author sareaboudousamadou.
 */
public record Fee(Amount amount, Currency currency) {

  public static Fee ZERO = new Fee(Amount.ZERO, Currency.EURO);

  public Fee {
    Assert.notNull("amount", amount);
    Assert.notNull("currency", currency);
  }

  public Fee times(Quantity quantity) {
    Assert.notNull("quantity", quantity);

    return new Fee(amount().times(quantity), currency());
  }

  public Fee add(Fee another) {
    Assert.notNull("another", another);

    return new Fee(amount.add(another.amount()), currency());
  }
}
