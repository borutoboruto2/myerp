package fr.samadou.myerp.invoice.domain;

import java.util.Optional;

/**
 * @author sareaboudousamadou.
 */
public interface InvoiceRepository {
  void save(Invoice invoice);

  Optional<Invoice> get(InvoiceId invoiceId);
}
