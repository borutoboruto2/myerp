package fr.samadou.myerp.invoice.domain;

import fr.samadou.myerp.error.domain.Assert;

import java.util.Collection;

/**
 * @author sareaboudousamadou.
 */
public record InvoiceToCreate(Collection<Line> lines) {
  public InvoiceToCreate {
    Assert.notEmpty("lines", lines);
  }

  public Invoice create() {

    return new Invoice(InvoiceId.newId(), lines);
  }
}
