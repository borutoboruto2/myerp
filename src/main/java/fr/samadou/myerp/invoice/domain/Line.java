package fr.samadou.myerp.invoice.domain;

import fr.samadou.myerp.error.domain.Assert;

public record Line(Quantity quantity, Fee unitPrice) {

	public Line {
		Assert.notNull("quantity", quantity);
		Assert.notNull("unitPrice", unitPrice);
	}

	Fee total() {
		return unitPrice()
				.times(quantity());
	}

  public Fee fee() {
    return unitPrice();
  }
}
