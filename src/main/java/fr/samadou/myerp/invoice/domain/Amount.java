package fr.samadou.myerp.invoice.domain;

import fr.samadou.myerp.error.domain.Assert;

import java.math.BigDecimal;

public record Amount(int value) {

	public static Amount ZERO = new Amount(0);

	public Amount {
		Assert.field("amount", 0).min(0);
	}

	public Amount times(Quantity qunatity) {
		Assert.notNull("quantity", qunatity);

		return new Amount(qunatity.value() * value());
	}

	public Amount add(Amount other) {
		Assert.notNull("other", other);

		return new Amount(value() + other.value());
	}

  public BigDecimal get() {
    return BigDecimal.valueOf(value());
  }
}
