package fr.samadou.myerp.invoice.domain;

import fr.samadou.myerp.error.domain.Assert;

/**
 * @author sareaboudousamadou.
 */
public class InvoiceCreator {

  private final InvoiceRepository invoices;

  public InvoiceCreator(InvoiceRepository invoices) {
    this.invoices = invoices;
  }

  public Invoice create(InvoiceToCreate invoiceToCreate) {
    Assert.notNull("invoiceToCreate", invoiceToCreate);

    var invoice = invoiceToCreate.create();

    invoices.save(invoice);

     return invoice;
  }
}
