package fr.samadou.myerp.invoice.domain;

import fr.samadou.myerp.error.domain.Assert;

public record Quantity(int value) {

	public Quantity {
		Assert.field("quantity", value)
		.min(1);
	}
}
