package fr.samadou.myerp.invoice.domain;

import fr.samadou.myerp.error.domain.Assert;

import java.util.Collection;

public class Invoice {

	private final Collection<Line> lines;
	private final InvoiceId id;

	public Invoice(InvoiceId id, Collection<Line> lines) {
		Assert.notNull("id", id);
		Assert.field("lines", lines)
		.notEmpty()
		.noNullElement();

		this.lines = lines;
		this.id = id;
	}

	public Fee total() {

		return lines().stream()
				.map(Line::total)
				.reduce(Fee.ZERO, Fee::add);
	}

	public Collection<Line> lines() {
		return lines;
	}

  public InvoiceId id() {
    return id;
  }
}
