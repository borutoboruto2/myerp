package fr.samadou.myerp.invoice.domain;

import java.util.UUID;

import fr.samadou.myerp.error.domain.Assert;

public record InvoiceId(UUID value) {

	public InvoiceId {
		Assert.notNull("invoiceId", value);
	}

  public static InvoiceId newId() {
    return new InvoiceId(UUID.randomUUID());
  }

  public UUID get() {
    return value;
  }
}
