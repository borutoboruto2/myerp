package fr.samadou.myerp;

import fr.samadou.myerp.common.domain.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
@Generated(reason = "Not testing logs test")
public class MyerpApp {

  private static final Logger log = LoggerFactory.getLogger(MyerpApp.class);

  public static void main(String[] args) {
    Environment env = SpringApplication.run(MyerpApp.class, args).getEnvironment();

    if (log.isInfoEnabled()) {
      log.info(ApplicationStartupTraces.of(env));
    }
  }
}
