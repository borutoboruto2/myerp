package fr.samadou.myerp.error.domain;

import java.io.Serializable;

public interface ErrorKey extends Serializable {
  String get();
}
