package fr.samadou.myerp.error.infrastructure.primary;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
class MyerpErrorsConfiguration {

  @Bean("applicationErrorMessageSource")
  MessageSource applictionErrorMessageSource() {
    ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();

    source.setBasename("classpath:/messages/errors/myerp-errors-messages");
    source.setDefaultEncoding("UTF-8");

    return source;
  }
}
